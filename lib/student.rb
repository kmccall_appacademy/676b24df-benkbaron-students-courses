class Student

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
    @name = "#{first_name} #{last_name}"
  end

  attr_reader :first_name, :last_name, :courses, :name

  def enroll(new_course)
    raise if courses.any? { |course| course.conflicts_with?(new_course) }

    unless courses.include?(new_course)
      courses << new_course
      new_course.students << self
    end
  end

  def course_load
    credit_hsh = Hash.new(0)
    courses.each { |course| credit_hsh[course.department] += course.credits }

    credit_hsh
  end

end

# ## Student
# * `Student#initialize` should take a first and last name.
# * `Student#name` should return the concatenation of the student's
#   first and last name.
# * `Student#courses` should return a list of the `Course`s in which
#   the student is enrolled.
# * `Student#enroll` should take a `Course` object, add it to the
#   student's list of courses, and update the `Course`'s list of
#   enrolled students.
#     * `enroll` should ignore attempts to re-enroll a student.
# * `Student#course_load` should return a hash of departments to # of
#   credits the student is taking in that department.
#
